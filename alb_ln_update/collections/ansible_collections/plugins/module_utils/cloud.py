from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


import random
from functools import wraps
import syslog
import time


def _exponential_backoff(retries=10, delay=2, backoff=2, max_delay=60):
    def backoff_gen():
        for retry in range(0, retries):
            sleep = delay * backoff ** retry
            yield sleep if max_delay is None else min(sleep, max_delay)
    return backoff_gen


def _full_jitter_backoff(retries=10, delay=3, max_delay=60, _random=random):
    def backoff_gen():
        for retry in range(0, retries):
            yield _random.randint(0, min(max_delay, delay * 2 ** retry))
    return backoff_gen


class CloudRetry(object):
    # This is the base class of the exception.
    # AWS Example botocore.exceptions.ClientError
    base_class = None

    @staticmethod
    def status_code_from_exception(error):
        pass

    @staticmethod
    def found(response_code, catch_extra_error_codes=None):
        pass

    @classmethod
    def _backoff(cls, backoff_strategy, catch_extra_error_codes=None):
        def deco(f):
            @wraps(f)
            def retry_func(*args, **kwargs):
                for delay in backoff_strategy():
                    try:
                        return f(*args, **kwargs)
                    except Exception as e:
                        if isinstance(e, cls.base_class):
                            response_code = cls.status_code_from_exception(e)
                            if cls.found(response_code, catch_extra_error_codes):
                                msg = "{0}: Retrying in {1} seconds...".format(str(e), delay)
                                syslog.syslog(syslog.LOG_INFO, msg)
                                time.sleep(delay)
                            else:
                                # Return original exception if exception is not a ClientError
                                raise e
                        else:
                            # Return original exception if exception is not a ClientError
                            raise e
                return f(*args, **kwargs)

            return retry_func  # true decorator

        return deco

    @classmethod
    def exponential_backoff(cls, retries=10, delay=3, backoff=2, max_delay=60, catch_extra_error_codes=None):
        return cls._backoff(_exponential_backoff(
            retries=retries, delay=delay, backoff=backoff, max_delay=max_delay), catch_extra_error_codes)

    @classmethod
    def jittered_backoff(cls, retries=10, delay=3, max_delay=60, catch_extra_error_codes=None):
        return cls._backoff(_full_jitter_backoff(
            retries=retries, delay=delay, max_delay=max_delay), catch_extra_error_codes)

    @classmethod
    def backoff(cls, tries=10, delay=3, backoff=1.1, catch_extra_error_codes=None):
        return cls.exponential_backoff(
            retries=tries - 1, delay=delay, backoff=backoff, max_delay=None, catch_extra_error_codes=catch_extra_error_codes)

